import os
import glob
import json
import urllib
import pathlib
import urllib.request
from distutils.version import LooseVersion

import yaml
import gitlab
from github import Github
from jinja2 import Environment, FileSystemLoader, BaseLoader, select_autoescape
from dockerfile_parse import DockerfileParser


gl = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['GITLAB_PERSONAL_TOKEN'])
gh = Github(os.environ['GITHUB_ACCESS_TOKEN'])

AUTOTOOLR_GROUP_ID = 6209348

def get_latest_tag(project):
    tags = project.tags.list()
    if len(tags) > 0:
        return tags[0].name
    return None

def get_project(project_id):
    return gl.projects.get(project_id)

def cleanup_versions(versions):
    return list(map(lambda v: v.replace('v', ''), versions))

def get_versions(definition):
    if definition['type'] == 'pip':
        url = f'https://pypi.python.org/pypi/{definition["package"]["name"]}/json'
        data = json.load(urllib.request.urlopen(url))
        versions = cleanup_versions(list(data['releases'].keys()))
        return sorted(versions, key=LooseVersion, reverse=True)
    elif definition['type'] == 'curl':
        url = definition['package']['version_url']
        data = urllib.request.urlopen(url).read().decode('utf-8')
        versions = cleanup_versions(data.splitlines())
        return versions
    elif definition['type'] == 'github':
        repo = gh.get_repo(definition['package']['repository'])
        versions = cleanup_versions(list(
            map(lambda r: r.tag_name,
                filter(lambda r: r.prerelease == definition['package']['use_prerelease'], repo.get_releases())
            )
        ))
        return sorted(versions, key=LooseVersion, reverse=True)
    elif definition['type'] == 'meta':
        defs = get_definitions()
        if definition['package']['version_master'] not in defs:
            return None
        return get_versions(defs[definition['package']['version_master']])
    return None

def get_definitions():
    def_array = []
    files = [f for f in glob.glob('definitions/*.yml')]
    for file in files:
        with open(file, 'r') as yaml_file:
            definition = yaml.safe_load(yaml_file)
            definition['_magic_sorter'] = 99 if definition['type'] == 'meta' else 1
            definition['_name'] = os.path.splitext(os.path.basename(file))[0]
            def_array.append(definition)
    
    sorted_defs = sorted(def_array, key=lambda d: d['_magic_sorter'])
    definitions = {}
    for definition in sorted_defs:
        definitions[definition['_name']] = definition

    return definitions


def get_repositories():
    autotoolr_group = gl.groups.get(AUTOTOOLR_GROUP_ID)
    return list(
        filter(lambda project: project.name != 'builder', autotoolr_group.projects.list())
    )

def create_repository(name):
    return gl.projects.create({
        'name': f'docker-{name}',
        'namespace_id': AUTOTOOLR_GROUP_ID,
        'visibility': 'public',
        'description': f'Automatically maintained docker image for {name}.',
        'issues_access_level': 'disabled',
        'merge_requests_access_level': 'disabled',
        'wiki_access_level': 'disabled',
        'snippets_access_level': 'disabled',
        'auto_devops_enabled': False
    })


def tag_version(project, version):
    return project.tags.create({
        'tag_name': version,
        'ref': 'master'
    })

def remove_tag(project, version):
    project.tags.delete(version)

def initialize_repository(project, definition):
    compiled_files = compile_files(project, definition)
    commit_actions = []
    for cfile in compiled_files:
        commit_actions.append({
            'action': 'create',
            'file_path': cfile['name'],
            'content': cfile['content']
        })

    return project.commits.create({
        'branch': 'master',
        'commit_message': 'Initial commit',
        'actions': commit_actions
    })

def compile_package_vars(project, definition):
    string_tpl_env = Environment(loader=BaseLoader(), autoescape=select_autoescape(['html', 'xml']))
    for key, value in definition['package'].items():
        try:
            compiled = string_tpl_env.from_string(value).render(**definition)
            definition['package'][key] = compiled
        except:
            pass
    return definition

def compile_files(project, definition):
    definition = compile_package_vars(project, definition)
    template_name = definition['type']
    tpl_env = Environment(
        loader=FileSystemLoader(f'templates/{template_name}'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    compiled_files = []
    template_files = [f for f in pathlib.Path(f'templates/{template_name}').glob('*')]
    for file in template_files:
        fname = os.path.basename(file)
        template = tpl_env.get_template(fname)
        rendered = template.render(
            repository_name=project.path,
            version=definition['version'],
            package=definition['package']
        )
        compiled_files.append({
            'name': fname,
            'content': rendered
        })

    return compiled_files

def prepare_definition(definition):
    if definition['type'] == 'meta':
        print(f'    Encountered meta package, downloading docker run-commands from included packages...')
        run_cmds = []
        for include in definition['package']['includes']:
            dfp = DockerfileParser()
            file_url = f'https://gitlab.com/autotoolr-docker/docker-{include}/raw/master/Dockerfile'
            print(f'    - {include} :: Fetching Dockerfile from {file_url}...')
            req = urllib.request.Request(
                file_url, data=None,
                headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0'
                }
            )
            dfp.content = urllib.request.urlopen(req).read().decode('utf-8')
            command = list(filter(lambda s: s['instruction'] == 'RUN', dfp.structure))[0]['value']
            run_cmds.append(f'RUN {command}')
            print(f'    - {include} :: Added RUN instruction to meta build')
        definition['package']['run_commands'] = run_cmds
    return definition

def update_files(project, definition, commit_message=None):
    compiled_files = compile_files(project, definition)
    commit_actions = []
    for cfile in compiled_files:
        commit_actions.append({
            'action': 'update',
            'file_path': cfile['name'],
            'content': cfile['content']
        })

    return project.commits.create({
        'branch': 'master',
        'commit_message': f'Version bump to {definition["version"]}' if commit_message is None else commit_message,
        'actions': commit_actions
    })