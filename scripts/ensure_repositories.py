import os

from .functions import (
    compile_files,
    update_files,
    get_definitions,
    prepare_definition,
    get_repositories,
    get_versions,
    create_repository,
    initialize_repository,
    tag_version, remove_tag,
    get_project, get_latest_tag
)


repos = get_repositories()
defs = get_definitions()

drifted_defs = []

for name, definition in defs.items():
    print(f'Checking {name}...')
    project_id = 0
    project = None
    init_commit = None
    versions = get_versions(definition)
    if len(versions) == 0:
        print(f'    Could not determine upstream version for {name}, aborting.')
        continue
    latest_upstream = versions[0]
    definition['version'] = latest_upstream
    print(f'    Determined upstream version {latest_upstream} for {name}.')

    # prepare definition
    definition = prepare_definition(definition)

    if len([rep for rep in repos if rep.name == f'docker-{name}']) == 0:
        # repository does not yet exist
        print(f'    Repository for {name} does not exist, creating...')
        project = create_repository(name)
        project_id = project.id
        init_commit = initialize_repository(project, definition)
        print(f'    Repository docker-{name} created and initial commit pushed.')
    
    print(f'    Found repository docker-{name}, checking tags and versions...')
    if project_id == 0:
        project_id = [rep for rep in repos if rep.name == f'docker-{name}'][0].id
    if project is None:
        project = get_project(project_id)

    # get latest tag
    last_tag = get_latest_tag(project)
    if last_tag is None or last_tag != latest_upstream:
        print(f'    Detected version drift ({last_tag} != {latest_upstream}) for docker-{name}, bumping version and tagging new release.')
        # version drift, re-generate files, issue update commit and push tag
        if init_commit is None:
            update_files(project, definition)
        tag_version(project, latest_upstream)
        drifted_defs.append(name)
    elif 'FORCE_TAG_PUSH' in os.environ and last_tag is not None:
        print(f'    Force tagging docker-{name} due to file changes.')
        remove_tag(project, last_tag)
        tag_version(project, last_tag)
    else:
        print(f'    Latest upstream {latest_upstream} already built, skipping {name}.')

# check all meta repositories for their drifted dependencies
if len(drifted_defs) > 0:
    print(f'Checking all drifted packages for dependencies in meta images...')
    for name, definition in defs.items():
        if definition['type'] != 'meta':
            continue
        # skip drifted definition that is deemed version master since that already ensure a bump on the meta package
        defs_to_check = list(filter(lambda dname: dname != definition['package']['version_master'], drifted_defs))
        if any(dname in definition['package']['includes'] for dname in defs_to_check):
            print(f'    Found downstream version drift affecting {name}, updating files to reflect change.')
            # get project        
            project = get_project([rep for rep in repos if rep.name == f'docker-{name}'][0].id)
            update_files(project, definition)