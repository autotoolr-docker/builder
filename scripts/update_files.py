import os
import pathlib

from .functions import get_definitions, get_repositories, update_files, get_project, prepare_definition


repos = get_repositories()
defs = get_definitions()

for name, definition in defs.items():
    if len([rep for rep in repos if rep.name == f'docker-{name}']) == 0:
        continue
    project_id = [rep for rep in repos if rep.name == f'docker-{name}'][0].id
    project = get_project(project_id)
    tags = project.tags.list()
    last_tag = None
    if len(tags) == 0:
        print(f'Could not find a pre-existing tag for {name}, skipping...')
        continue

    last_tag = tags[-1].name
    definition['version'] = last_tag
    print(f'Found version {last_tag} for {name}, updating source files for release...')
    definition = prepare_definition(definition)
    update_files(project, definition, 'Updating source files based on template change')