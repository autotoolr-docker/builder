#!/bin/bash
if [ -f ".force_tag_push" ]; then
    FORCE_TAG_PUSH=true python3 -m scripts.ensure_repositories
else
    python3 -m scripts.ensure_repositories
fi