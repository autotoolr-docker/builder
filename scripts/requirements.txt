python-gitlab==1.11.0
pyyaml==5.1.2
Jinja2==2.10.1
PyGithub==1.43.8
dockerfile-parse==0.0.15