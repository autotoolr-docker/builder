# Autotoolr Docker
Automatically built and maintained Docker images for the most popular CLI's. Never look for up to date CLI images for your CI/CD needs again, just use Autotoolrs `latest` tag.
## How does it work?
The builder-repository (the one you are currently looking at) controls and maintains all other repositories. The pipeline is run every night at 04:00 to check all registered CLI's for their currently tagged and built version against the upstream version.

The folder `definitions` contains all CLI's built using autotoolr. There are several providers available to actually fetch the version and the binary for it.

## Definition providers
Definitions are written in YAML with 2 main attributes: `type` and `package`. The attribute `package` contains the type specific configuration values described for each definition type below.

__IMPORTANT__: If you implement a new provider, make sure it's consisting of __EXACTLY__ one `RUN` instruction within the `Dockerfile`. If you provide multiple `RUN` instructions, meta images cannot build correctly!

### pip
Using the pip-type, you can install a CLI using pip (python's pacakge manager). The image installs `python3`, so make sure your desired CLI supports that. As a baseline, the image also installs the packages `pip`, `setuptools` and `wheel`. For an example definition, see `aws-cli`.

| Property | Description | Required | Default Value
|:-|:-|:-:|:-|
| `name` | The name of the package | Yes | n/a
| `additional_packages` | If you need additional supporting packages, state them here (seperated by whitespace) | No | n/a

### curl
This type installs a binary directly using curl and expects the current version(s) to be available as a plain text file via HTTP/HTTPS. For an example definition, see `kubectl`.

| Property | Description | Required | Default Value
|:-|:-|:-:|:-|
| `version_url` | The URL for the version check file (must be text/plain) | Yes | n/a
| `url` | The URL to the binary (can contain {{ version }} as a placeholder) | Yes | n/a
| `exec_name` | The name of the executable file | Yes | n/a
| `unpack_command` | Command to unpack an intermediate format | No | `None`

### github
This type installs a binary using curl and and determines the current version using GitHub Releases API. For an example definition, see `helm`.

| Property | Description | Required | Default Value
|:-|:-|:-:|:-|
| `repository` | The URL for the version check file (must be text/plain) | Yes | n/a
| `url` | The URL to the binary (can contain {{ version }} as a placeholder) | Yes | n/a
| `exec_name` | The name of the executable file | Yes | n/a
| `use_prerelease` | Use pre-releases instead of stable | No | `false`
| `unpack_command` | Command to unpack an intermediate format | No | `None`

### meta
Using this type, you can combine an arbitrary number of defined standalone CLI's.

| Property | Description | Required | Default Value
|:-|:-|:-:|:-|
| `includes` | A list of package names that should be included in this package | Yes | n/a
| `version_master` | The package on which the version of this meta package is based and tagged on | Yes | n/a

## Adding a new CLI
If you want to add a CLI you think is missing, follow these steps:
1) Create a new branch (name should be explanatory, i.e. the CLI's name)
2) Add a new definition file using one of the providers above.
3) Push your branch.
4) Open up a merge request for your branch and wait for review.
5) As soon as your request was merged, the new repository will be automatically created and the first image will be built.