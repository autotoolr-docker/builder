FROM alpine:latest

LABEL maintainer="Autotoolr <https://gitlab.com/autotoolr-docker>" cli_version="0.12.10"

RUN apk -v --update add ca-certificates && \
    apk add --virtual=build curl tar zip gzip && \
    curl -LO https://releases.hashicorp.com/terraform/0.12.10/terraform_0.12.10_linux_amd64.zip && \
    unzip terraform_0.12.10_linux_amd64.zip && \
    chmod +x ./terraform && \
    mv ./terraform /usr/local/bin/terraform && \
    apk --purge del build && \
    rm /var/cache/apk/*